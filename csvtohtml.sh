#!/bin/bash

directory=$(dirname $0)

[ $# -lt 2 ] && { echo "Usage: $0 input.csv output.html"; exit 1; }

if([[ "$1" =~ ".csv" ]] && [[ "$2" =~ ".html" ]])
then
    php $directory/index.php $@
else
    echo "Input must be a csv file and the output must be an html file!";
    exit 1;
fi
