<?php
    /** script for parsing a csv file to html file through PHP cli */

    /** Mapping an csv file to an array.
        @param string $path full path to a csv file
     */
    function get_csv($path){
        $csv = array_map('str_getcsv', file($path));
        return $csv;
    }

    //generate the beginning of a html file.
    function html_beggining(){
        return "<html><head><!-- header --></head><body><table>\r\n";
    }

    //generate the end of a html file.
    function html_ending(){
        return "</table></body></html>\r\n";
    }


    /** Creating a html file from the csv mapped array.
        Each row and line of the csv is parsed into a html table.
        @param string $file path to an output.html file.
        @param array $csv_mappedarray multidimensional array containing the csv rows.
    */
    function parse_cv_tohtml($csv_mappedarray,$file){
        $i = 0;
        fwrite($file,html_beggining());
        foreach($csv_mappedarray as $key=>$value){
            fwrite($file,'
                    <tr><td><div id='.$i.'><div class="date">'.$value[0].
                    '</div></td><td><div class="nick">'.$value[1].
                    '</div></td><td><div class="message">'.$value[2].
                    '</div></div></td></tr>');
            $i++;
        }
        fwrite($file,html_ending());
    }

    $file = fopen($argv[2],'w');
    parse_cv_tohtml(get_csv($argv[1]),$file);
?>
